

# Host Foreground
$Host.PrivateData.ErrorForegroundColor = 'Red'
$Host.PrivateData.WarningForegroundColor = 'Magenta'
$Host.PrivateData.DebugForegroundColor = 'Black'
$Host.PrivateData.VerboseForegroundColor = 'Blue'
$Host.PrivateData.ProgressForegroundColor = 'White'

# Host Background
$Host.PrivateData.ErrorBackgroundColor = 'DarkYellow'
$Host.PrivateData.WarningBackgroundColor = 'DarkYellow'
$Host.PrivateData.DebugBackgroundColor = 'DarkYellow'
$Host.PrivateData.VerboseBackgroundColor = 'DarkYellow'
$Host.PrivateData.ProgressBackgroundColor = 'DarkBlue'

# New version of PSReadline renames Foreground colors and eliminates Background
Set-PSReadlineOption -Colors @{
  Command = 'DarkRed'
  Comment = 'DarkGray'
  ContinuationPrompt = 'DarkCyan'
  DefaultToken = 'DarkMagenta'
  Emphasis = 'Red'
  Error = 'DarkRed'
  Keyword = 'DarkGreen'
  Member = 'DarkGreen'
  Number = 'DarkGreen'
  Operator = 'Black'
  Parameter = 'DarkCyan'
  String = 'DarkBlue'
  Type = 'DarkBlue'
  Variable = 'DarkGreen'
}

Set-Location C:\Work\

$ErrorActionPreference = 'Continue'

function Get-MyUptime {
  $thisComp = "LocalHost"
  $osBooted = Get-WmiObject -Class Win32_OperatingSystem -ComputerName $thisComp
  # $dateNow = [DateTime]::Now
  $dateNow = Get-Date
  $dateBooted = $osBooted.ConvertToDateTime($osBooted.LastBootUpTime)
  $bootTimeDiff = $dateNow - $dateBooted
  if ($($bootTimeDiff.Days) -eq 0) {
    $upDays = ''
  } elseif ($($bootTimeDiff.Days) -eq 1) {
    $upDays = "$($bootTimeDiff.Days) day, "
  } else {
    $upDays = "$($bootTimeDiff.Days) days, "
  }
  Write-Host "`nBooted at:`t$("{0:d2}" -f $($dateBooted.Day)).$("{0:d2}" -f $($dateBooted.Month)).$($dateBooted.Year) $("{0,2}" -f $($dateBooted.Hour)):$("{0:d2}" -f $($dateBooted.Minute)):$("{0:d2}" -f $($dateBooted.Second))" -ForegroundColor DarkGreen
  Write-Host "Now:`t`t$("{0:d2}" -f $($dateNow.Day)).$("{0:d2}" -f $($dateNow.Month)).$($dateNow.Year) $("{0,2}" -f $($dateNow.Hour)):$("{0:d2}" -f $($dateNow.Minute)):$("{0:d2}" -f $($dateNow.Second))"
  Write-Host "Uptime:`t`t$upDays$("{0,2}" -f $($bootTimeDiff.Hours)):$("{0:d2}" -f $($bootTimeDiff.Minutes)):$("{0:d2}" -f $($bootTimeDiff.Seconds))`n" -ForegroundColor DarkBlue
}

function Get-MyIP {
  try {
    $myIP = Invoke-RestMethod -Uri 'http://ipinfo.io/json' -ErrorAction Stop | Select-Object -ExpandProperty ip
    $myIPLocal = (Get-WmiObject -ComputerName $env:COMPUTERNAME -ClassName Win32_NetworkAdapterConfiguration -Filter "IPEnabled = 'True'"  | Select-Object -Property *).IPAddress[0]
    Write-Host "`nLocal:`t`t$myIPLocal"
    Write-Host "Internet:`t$myIP`n" -ForegroundColor DarkBlue
  } catch {
    Write-Host "`nThe ip-checking website is temporarily unavailable.`n" -ForegroundColor DarkRed
  }
}

function Get-MyWeather {
  try{
    $myWeather = Invoke-RestMethod -Uri 'https://api.openweathermap.org/data/2.5/weather?id=524901&appid=d6843ab8ee963f5d372296dfff62aed7' -ErrorAction Stop
    [Int] $myTempC = $myWeather.main.temp - 273.15
    # 1 hPa = 100 Pa (hPa is hectopascal)
    # [Int] $myPressure = $myWeather.main.pressure * 100 * 760 / 101325
    [Int] $myPressure = $myWeather.main.pressure * 76000 / 101325
    [String] $myWind = $myWeather.wind.speed
    [String] $myGust = $myWeather.wind.gust
    [String] $mySnowRain = $myWeather.weather.description
    [String] $myClouds = $myWeather.clouds.all
    [String] $myHumidity = $myWeather.main.humidity
    [datetime] $mySunrise = (Get-Date 01.01.1970).AddSeconds($myWeather.sys.sunrise).ToLocalTime()
    [datetime] $mySunset = (Get-Date 01.01.1970).AddSeconds($myWeather.sys.sunset).ToLocalTime()
    [Int] $myVisibility = $myWeather.visibility / 100
    
    Write-Host "`nMoscow, Russia Weather" -ForegroundColor DarkGreen
    Write-Host "-----------------------------------"
    Write-Host "Temperature:`t$myTempC C"
    Write-Host "Pressure:`t$myPressure" 
    Write-Host "Wind:`t`t$myWind m`/s"
    if ($myGust) {
      Write-Host "Gust:`t`t$myGust m`/s"
    }
    Write-Host "Precipitation:`t$mySnowRain"
    Write-Host "Sky covered:`t$myClouds %"
    Write-Host "Humidity:`t$myHumidity %"
    if ($myVisibility -lt 100) {
      Write-Host "Visibility:`t$myVisibility %"
    }
    Write-Host "Sunrise:`t$("{0:d2}" -f $($mySunrise.Day)).$("{0:d2}" -f $($mySunrise.Month)).$($mySunrise.Year) $("{0:d2}" -f $($mySunrise.Hour)):$("{0:d2}" -f $($mySunrise.Minute)):$("{0:d2}" -f $($mySunrise.Second))"
    Write-Host "Sunset:`t`t$("{0:d2}" -f $($mySunset.Day)).$("{0:d2}" -f $($mySunset.Month)).$($mySunset.Year) $("{0:d2}" -f $($mySunset.Hour)):$("{0:d2}" -f $($mySunset.Minute)):$("{0:d2}" -f $($mySunset.Second))"
    Write-Host "-----------------------------------`n"
  } catch {
    Write-Host "`nThe weather website is temporarily unavailable.`n" -ForegroundColor DarkRed
  }
}

function Get-MyTranslation {
  param(
    [Parameter(Mandatory)]
    [String] $SourceText,
    [Parameter()]
    [ValidateSet('en-ru', 'ru-en')]
    [String] $Direction = 'en-ru'
  )
  try {
    Add-Type -AssemblyName "System.Web"
    $SourceEncoded = [System.Web.HttpUtility]::UrlEncode($SourceText)
    $myTranslation = Invoke-RestMethod "https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20200501T032725Z.bf9d6b4bf0d6124f.958e3e4703e51c118ee0ad38396fe5abd44eef0a&text=$SourceEncoded&lang=$Direction" -ErrorAction Stop | Select-Object -ExpandProperty text
    $myTranslation = [System.Web.HttpUtility]::UrlDecode($myTranslation)
    Write-Host "`n$SourceText`n"
    Write-Host "$myTranslation`n" -ForegroundColor DarkBlue
  } catch {
    Write-Host "`nThe translation website is temporarily unavailable.`n" -ForegroundColor DarkRed
  }
}

Clear-Host

